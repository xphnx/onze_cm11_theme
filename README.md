<!-- disabling until having a right gradlew file    ![CI bagde](https://gitlab.com/xphnx/onze_cm11_theme/badges/master/build.svg) -->

### The task of keeping the code updated to have all icons up to date and adding new icon applications has become very tedious for me. For this reason, at the moment, this repository will be **UNMAINTAINED**. 
If someone was willing to deal with the code updates I could commit to add new designs, otherwise it is impossible for me. As always, feel free to fork the repos.

## Onze CM11 Theme for FLOSS apps ##

Onze is a fork of [TwelF](https://gitlab.com/xphnx/twelf_cm12_theme/tree/master) a Material Design inspired theme aiming to provide a consistent and minimalistic look to your device. This theme only supports icons for FLOSS apps. Requires a Cyanogenmod 11 Theme Engine compatible ROM.
For CM12/CM13 devices take a look at [TwelF Theme](https://gitlab.com/xphnx/twelf_cm12_theme).

You can use [Turtl](https://f-droid.org/repository/browse/?fdid=org.xphnx.iconsubmit) or the [TwelF issue tracker](https://gitlab.com/xphnx/twelf_cm12_theme/issues) for requesting icons. As determining if the icons requests are related to FLOSS apps, currently only [F-droid](https://f-droid.org/) hosted apps are supported.

# Features #

* Icon Pack
* Wallpaper & Lockscreen 
* Alarm & Ringtone

[Changelog] (https://gitlab.com/xphnx/onze_cm11_theme/blob/master/CHANGELOG.md)

[![Get_it_on_F-Droid.svg](https://gitlab.com/uploads/xphnx/twelf_cm12_theme/a4649863bd/Get_it_on_F-Droid.svg.png)](https://f-droid.org/app/org.tw.onze.cmtheme)

Licenses [GPLv3](http://www.gnu.org/licenses/gpl-3.0.html) and other libre or open licenses for the artwork (see [CREDITS](https://gitlab.com/xphnx/onze_cm11_theme/blob/master/CREDITS.md) for more details).

# Snapshots #

![Screenshot12](https://gitlab.com/xphnx/twelf_cm12_theme/uploads/d87bee1f24b48660e6f8f364adadef11/Screenshot12.png)
